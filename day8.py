import pytest
import pathlib


class Node:
    def __init__(self, children, metadata):
        self.child_nodes = children
        self.metadata = metadata

    @property
    def metadata_length(self):
        return len(self.metadata)

    @property
    def children(self):
        return len(self.child_nodes)

    @property
    def sum(self):
        s = sum(self.metadata)
        for child in self.child_nodes:
            s += child.sum
        return s

    @property
    def sum2(self):
        if self.children == 0:
            return self.sum
        else:
            s = 0
            for m in self.metadata:
                try:
                    s += self.child_nodes[m-1].sum2
                except IndexError:
                    pass
            return s


def get_metadata(values, n):
    m = []
    while n > 0:
        m.append(values.pop(0))
        n -= 1
    return m


def construct_tree(values):
    children = values.pop(0)
    metadata = values.pop(0)
    c = []
    while children > 0:
        c.extend(construct_tree(values))
        children -= 1
    m = get_metadata(values, metadata)
    return [Node(c, m)]


def to_list(string):
    return [int(i) for i in string.split(" ")]


class TestNode:
    def test_node_has_0_children_and_0_metadata(self):
        n = Node([], [])
        assert n.metadata == []
        assert n.child_nodes == []

    def test_length_of_zero_metadata_is_zero(self):
        n = Node([], [])
        assert n.metadata_length == 0

    def test_node_has_0_children_and_1_metadatum(self):
        n = Node([], [1])
        assert n.metadata_length == 1

    def test_node_has_two_items_of_metadata(self):
        n = Node([], [1, 2])
        assert n.metadata_length == 2

    def test_node_has_1_2_in_metadata(self):
        n = Node([], [1, 2])
        assert n.metadata == [1, 2]

    def test_node_has_child_node(self):
        child = Node([], [])
        n = Node([child], [])
        assert n.child_nodes == [child]

    def test_node_has_one_child(self):
        child = Node([], [])
        n = Node([child], [])
        assert n.children == 1

    def test_sum_of_1_2_is_3(self):
        assert Node([], [1, 2]).sum == 3

    def test_sum_of_1_1_2_is_4(self):
        assert Node([], [1, 1, 2]).sum == 4

    def test_sum_of_two_nodes_is_combined(self):
        assert Node([Node([], [1, 1])], [99]).sum == 101

    def test_sum2_with_no_children_is_same(self):
        assert Node([], [10, 11, 12]).sum2 == 33

    def test_sum2_with_one_child_but_metadata_2_returns_zero(self):
        assert Node([Node([], [1])], [2]).sum2 == 0

    def test_sum2_with_one_child_with_metadata_1_returns_1(self):
        assert Node([Node([], [1])], [1]).sum2 == 1


class TestListConstruction:
    def test_0_0_gives_one_node_with_no_data(self):
        assert len(construct_tree([0, 0])) == 1

    def test_0_0_gives_one_node_with_no_metadata(self):
        assert construct_tree([0, 0])[0].metadata_length == 0

    def test_0_0_gives_one_node_with_no_children(self):
        assert construct_tree([0, 0])[0].children == 0

    def test_0_1_2_gives_one_node_with_one_metadatum_of_value_2(self):
        assert construct_tree([0, 1, 2])[0].metadata == [2]

    def test_0_2_3_4_gives_one_node_with_two_data_containing_3_4(self):
        assert construct_tree([0, 2, 3, 4])[0].metadata == [3, 4]


class Test_1_3_0_3_10_11_12_1_1_2:
    @pytest.fixture
    def tree(self):
        return construct_tree([1, 3, 0, 3, 10, 11, 12, 1, 1, 2])

    def test_gives_one_root_node(self, tree):
        assert len(tree) == 1

    def test_root_has_one_child(self, tree):
        assert tree[0].children == 1

    def test_child_has_10_11_12(self, tree):
        assert tree[0].child_nodes[0].metadata == [10, 11, 12]

    def test_root_has_1_1_2(self, tree):
        assert tree[0].metadata == [1, 1, 2]

    def test_sum2_is_66(self, tree):
        answer = 10+11+12+10+11+12
        assert tree[0].sum2 == answer


class TestExample:
    @pytest.fixture
    def tree(self):
        return construct_tree(to_list("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"))

    def test_to_list(self):
        assert to_list("2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2") == [2, 3, 0, 3, 10, 11, 12, 1, 1, 0, 1, 99, 2, 1, 1, 2]

    def test_one_root(self, tree):
        assert len(tree) == 1

    def test_root_has_two_children(self, tree):
        assert tree[0].children == 2

    def test_sum_is_138(self, tree):
        assert tree[0].sum == 138

    def test_sum2_is_66(self, tree):
        assert tree[0].sum2 == 66


class TestInput:
    @pytest.fixture
    def tree(self):
        return construct_tree(to_list(pathlib.Path(r"D:\AOC2018\input\day8.txt").read_text()))

    def test_sum(self, tree):
        assert tree[0].sum == 46781

    def test_sum2(self, tree):
        assert tree[0].sum2 == 21405

