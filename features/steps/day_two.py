import behave
import itertools
import collections

def find_duplicates(word):
    counted = collections.Counter(word).most_common()
    twos = list(filter(lambda x: x[1]==2, counted))
    threes = list(filter(lambda x: x[1]==3, counted))
    return len(twos)>0, len(threes)>0

@behave.given('the input word \'{word}\'')
def set_input(context, word):
    context.input = word

@behave.when('look for duplicates')
def find_duplicates_step(context):
    context.two, context.three = find_duplicates(context.input)

@behave.then('does not contain a two')
def two_check(context):
    assert not context.two

@behave.then('does contain a two')
def two_check(context):
    assert context.two

@behave.then('does contain a three')
def three_check(context):
    assert context.three

@behave.then('does not contain a three')
def three_check(context):
    assert not context.three

