import itertools
import behave
import pathlib


def change_frequencies(input, start=None):
    res = start or [0]
    for val in input:
        res.append(res[-1] + int(val))
    return res

def get_final_frequency(input):
    return change_frequencies(input)[-1]


def test_add_0_1_returns_1():
    assert 1 == get_final_frequency([0, 1])


def test_add_1_m2_returns_m1():
    assert -1 == get_final_frequency([1, -2])


def find_first_duplicate(input):
    frequencies = [0]

    for val in itertools.cycle(input):
        new = frequencies[-1] + int(val)
        if new in frequencies:
            return new
        else:
            frequencies.append(new)


@behave.given('the input is "{input}"')
def set_string(context, input):
    context.input = input


@behave.when('the file is opened')
def open_file(context):
    context.input = pathlib.Path(context.input).read_text()


@behave.when('the input is formatted')
def format_input(context):
    context.input = context.input.replace("\n", ",")
    context.input = [i for i in context.input.split(',') if i != '']


@behave.when('change the frequency')
def change_the_frequency(context):
    context.result = get_final_frequency(context.input)

@behave.when('change the frequency until a duplicate is found')
def find_the_duplicate(context):
    context.result = find_first_duplicate(context.input)


@behave.then('the result is {result:d}')
def step_impl(context, result):
    print(context.result)
    assert context.result == result
