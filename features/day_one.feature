Feature: Day one
  Scenario: +1, +1, +1 results in  3
    Given the input is "+1, +1, +1"
    When the input is formatted
    And change the frequency
    Then the result is 3

  Scenario: +1, +1, -2 results in  0
    Given the input is "+1, +1, -2"
    When the input is formatted
    And change the frequency
    Then the result is 0

  Scenario: -1, -2, -3 results in -6
    Given the input is "-1, -2, -3"
    When the input is formatted
    And change the frequency
    Then the result is -6

  Scenario: The input file gives answer
    Given the input is "input/day_one.txt"
    When the file is opened
    And the input is formatted
    And change the frequency
    Then the result is 439

  Scenario: +1, -1 first reaches 0 twice
    Given the input is "+1, -1"
    When the input is formatted
    And change the frequency until a duplicate is found
    Then the result is 0

  Scenario: +3, +3, +4, -2, -4 first reaches 10 twice
    Given the input is "+3, +3, +4, -2, -4"
    When the input is formatted
    And change the frequency until a duplicate is found
    Then the result is 10

  Scenario: the input file gives answer to part two
    Given the input is "input/day_one.txt"
    When the file is opened
    And the input is formatted
    And change the frequency until a duplicate is found
    Then the result is 124645
