Feature: Find number of words with two duplicate letters and three duplicate letters.
         Multiply them together to form a checksum.

  Scenario: abcdef contains no letters that appear exactly two or three times
    Given the input word 'abcdef'
    When look for duplicates
    Then does not contain a two
    And does not contain a three

   Scenario: bababc contains two a and three b, so it counts for both.
     Given the input word 'bababc'
     When look for duplicates
     Then does contain a two
     And does contain a three

  Scenario: abbcde contains two b, but no letter appears exactly three times
    Given the input word 'abbcde'
    When look for duplicates
    Then does contain a two
    And does not contain a three

  Scenario: abcccd contains three c, but no letter appears exactly two times
    Given the input word 'abcccd'
    When look for duplicates
    Then does not contain a two
    And does contain a three

  Scenario: aabcdd contains two a and two d, but it only counts once
    Given the input word 'aabcdd'
    When look for duplicates
    Then does contain a two
    And does not contain a three

  Scenario: abcdee contains two e
    Given the input word 'abcdee'
    When look for duplicates
    Then does contain a two
    And does not contain a three

  Scenario: ababab contains three a and three b, but it only counts once
    Given the input word 'ababab'
    When look for duplicates
    Then does not contain a two
    And does contain a three

